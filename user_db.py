import datetime
import hashlib
import os
from typing import List, Optional

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from sqlalchemy.orm import relationship, Session
import sqlalchemy

# an Engine, which the Session will use for connection
# resources
import app

engine = create_engine(f'postgresql://test:testpass@{os.environ["host"]}:5433/db',
                       echo=True,
                       future=True,
                       pool_recycle=300,
                       pool_pre_ping=True,
                       pool_use_lifo=True)
# engine = create_engine('postgresql://test:testpass@host.docker.internal:5433/db', echo=True, future=True)
Base = declarative_base()


class _User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    password = Column(String(60), nullable=False)
    groups = relationship("_Member", back_populates="user")
    logs = relationship("_Log", back_populates="user")


class _Group(Base):
    __tablename__ = 'group'
    id = Column(Integer, primary_key=True)
    group_name = Column(String, nullable=False)
    mode = Column(Integer, nullable=False)
    members = relationship("_Member", back_populates="group")


class _Member(Base):
    __tablename__ = 'member'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    group_id = Column(Integer, ForeignKey('group.id'))
    user = relationship("_User", back_populates="groups")
    group = relationship("_Group", back_populates="members")
    name = Column(String(60), nullable=False)
    group_name = Column(String(60), nullable=False)


class _Log(Base):
    __tablename__ = 'logs'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("_User", back_populates="logs")
    timestamp = Column(DateTime, nullable=False)
    msg = Column(String, nullable=False)

    def __repr__(self):
        return f"<Log: {self.timestamp} - {self.msg[:50]}>"


class User:

    def __init__(self, user: _User):
        self.id = user.id
        self.name = user.name
        self.password = user.password

        self.groups = [g.group_name for g in user.groups]
        print(self.groups)

    def __repr__(self):
        return self.name


# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)


def user_exist(name: str) -> bool:
    with Session(engine) as s:
        d = s.execute(select(_User).filter(_User.name == name))
        if d.first():
            return True
        return False


def get_user(name: str) -> Optional[User]:
    with Session(engine) as s:
        try:
            user = s.execute(select(_User).filter_by(name=name)).scalar_one()
            return User(user)
        except sqlalchemy.exc.NoResultFound:
            return None


def get_user_by_id(user_id: int) -> User:
    with Session(engine) as s:
        user = s.get(_User, user_id)
        return User(user)


def add_user(name: str, password: str, groups: List[str]):
    with Session(engine) as s:
        if not user_exist(name):
            user = _User(name=name, password=password)

            for group_name in [name, *groups]:
                group = get_group(group_name)
                if not group:
                    group = _Group(group_name=group_name, mode=3)
                member = _Member(user_id=user.id, group_id=group.id, group_name=group.group_name, name=user.name)
                user.groups.append(member)
                group.members.append(member)
                s.add(group)
                s.add(member)
            s.add(user)
            s.commit()


def log(user_id: int, msg: str):
    with Session(engine) as s:
        new_log = _Log(user_id=user_id, msg=msg, timestamp=datetime.datetime.now())
        s.add(new_log)
        s.commit()


def all_log() -> List[_Log]:
    with Session(engine) as s:
        logs = s.execute(select(_Log)).scalars()
        return [log for log in logs]


def user_log(user_id: int) -> List[_Log]:
    with Session(engine) as s:
        logs = s.execute(select(_Log).filter_by(user_id=user_id)).scalars()
        return [log for log in logs]


def get_membership(user, group, create=False) -> Optional[_Member]:
    with Session(engine) as s:

        member = s.execute(select(_Member).filter_by(group_name=group.group_name).filter_by(name=user.name)).scalars()
        try:
            return next(member)
        except StopIteration:
            if not create:
                return None
            else:
                member = _Member(user_id=user.id, group_id=group.id, group_name=group.group_name, name=user.name)
                user.groups.append(member)
                group.members.append(member)
                return member


def update_user(user: User):
    with Session(engine) as s:
        db_user = s.get(_User, user.id)
        db_user.name = user.name
        db_user.id = user.id
        db_user.password = user.password
        [get_membership(db_user, get_group(group_name), create=True) for group_name in user.groups]
        s.commit()


def delete_user(name: str):
    with Session(engine) as s:
        user = s.execute(select(_User).filter_by(name=name)).scalar_one()
        s.delete(user)
        s.commit()


def get_all_users(raw=False):
    with Session(engine) as s:
        users = s.execute(select(_User)).scalars()
        if not raw:
            return [User(user) for user in users]
        return [user for user in users]


def get_all_groups():
    with Session(engine) as s:
        groups = s.execute(select(_Group)).scalars()
        return [group for group in groups]


def get_all_groups_by_mode(mode: int):
    with Session(engine) as s:
        groups = s.execute(select(_Group).filter_by(mode=mode)).scalars()
        return [group for group in groups]


def get_group(group_name: str) -> Optional[_Group]:
    with Session(engine) as s:
        group = s.execute(select(_Group).filter_by(group_name=group_name)).scalars()
        try:
            group = next(group)
            [g for g in group.members]
            return group
        except StopIteration:
            return None


def get_group_by_id(user_id: int) -> User:
    with Session(engine) as s:
        user = s.get(_Group, user_id)
        return user


def get_users_group(group_name: str) -> List[User]:
    with Session(engine) as s:
        group = get_group(group_name)
        members = s.execute(select(_Member).filter_by(group_id=group.id)).scalars()
        return [User(m.user) for m in members]


def add_group(group_name: str, mode: int, user: User):
    with Session(engine) as s:
        db_user = s.get(_User, user.id)
        group = get_group(group_name)
        if not group:
            group = _Group(group_name=group_name, mode=mode)
        membership = get_membership(db_user, group, create=True)
        s.add(membership)
        s.add(group)
        s.add(db_user)
        s.commit()


# add test user
def _add_test_user():
    with Session(engine) as s:
        password = hashlib.sha224("test".encode('utf-8')).hexdigest()
        name = "test"
        groups = [name, 'admin', 'observer', 'user']
        if not user_exist(name):
            user = _User(name=name, password=password)
            for group_name in groups:
                group = get_group(group_name)
                if not group:
                    group = _Group(group_name=group_name, mode=3)
                member = get_membership(user, group, create=True)
                s.add(group)
                s.add(member)

            s.add(user)
            s.commit()


_add_test_user()
print(get_all_users())
print(get_all_groups())
