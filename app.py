import datetime
import hashlib
import os
import shutil
import threading
import traceback
from functools import wraps
from zipfile import ZipFile

import flask_login
import requests as requests
from flask import render_template, request, Flask, redirect, url_for, send_from_directory
import redis_queue

import job_db
import user_db

app = Flask(__name__)
app.secret_key = 'ksdjfk83jdJJKD9fd'

login_manager = flask_login.LoginManager()
login_manager.init_app(app)

"""
Flask_Utility
"""


class User(flask_login.UserMixin):
    pass


def get_flask_user(user):
    flask_user = User()
    flask_user.__dict__.update(user.__dict__)
    return flask_user


#
# def get_flask_user(user):
#     flask_user = User()
#     flask_user.id = user['_source']['username']
#     flask_user.admin = user['_source']['admin']
#     flask_user.user = user['_source']['user']
#     flask_user.observer = user['_source']['observer']
#     try:
#         flask_user.agent = user['_source']['agent']
#     except:
#         flask_user.agent = False
#     return flask_user


def check_admin(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not flask_login.current_user.is_authenticated or not 'admin' in flask_login.current_user.groups:
            return redirect(url_for('home'))
        return func(*args, **kwargs)

    return decorated_view


def check_observer(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask_login.current_user.is_authenticated and 'admin' in flask_login.current_user.groups or 'observer' in flask_login.current_user.groups:
            return func(*args, **kwargs)
        return redirect(url_for('home'))

    return decorated_view


def check_user(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask_login.current_user.is_authenticated and 'admin' in flask_login.current_user.groups or 'user' in flask_login.current_user.groups:
            return func(*args, **kwargs)
        return redirect(url_for('home'))

    return decorated_view


def check_aosint(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask_login.current_user.is_authenticated and 'admin' in flask_login.current_user.groups or 'user' in flask_login.current_user.groups or 'aosint' in flask_login.current_user.groups:
            return func(*args, **kwargs)
        return redirect(url_for('home'))

    return decorated_view


@login_manager.user_loader
def user_loader(user_id):
    user = user_db.get_user_by_id(user_id)
    return get_flask_user(user)


def log(msg):
    user_id = flask_login.current_user.id
    user_db.log(user_id, msg)


"""
Flask
"""


@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect(url_for('login'))


@app.route('/create_user', methods=['POST'])
@check_admin
def create_user():
    username = request.form.get('username')
    password = hashlib.sha224(request.form.get('password').encode('utf-8')).hexdigest()

    groups = []
    if request.form.get('admin'):
        groups.append("admin")
    if request.form.get('observer'):
        groups.append("observer")
    if request.form.get('user'):
        groups.append("user")

    user_db.add_user(username, password, groups)
    log(f"creating user: {username}")
    return admin()


@app.route('/delete_user')
@check_admin
def delete_user():
    username = request.args.get('username')
    user_db.delete_user(username)
    log(f"deleting user: {username}")
    return redirect(url_for('admin'))


@app.route('/')
@app.route('/home')
@flask_login.login_required
def home():
    return render_template("choose.html", modules=job_db.get_all_modules(), user=flask_login.current_user)


@app.route('/admin')
@check_admin
def admin():
    return render_template('util/admin.html', users=user_db.get_all_users(), user=flask_login.current_user)


@app.route('/settings', methods=['GET', 'POST'])
@flask_login.login_required
def settings():
    if request.method == "GET":
        return render_template('util/settings.html', user=flask_login.current_user)
    password = request.form.get('password')
    user = flask_login.current_user
    user.password = hashlib.sha224(password.encode('utf-8')).hexdigest()
    user_db.update_user(user)
    log("change password")
    return redirect(url_for('home'))


@app.route('/user_log')
@flask_login.login_required
def user_log():
    logs = user_db.all_log()
    logs = reversed(logs)
    return render_template('util/user_logs.html', logs=logs, user=flask_login.current_user)


@app.route('/jobs')
@flask_login.login_required
def jobs():
    jobs = job_db.get_all_jobs()
    jobs = reversed(jobs)
    return render_template('util/jobs.html', jobs=jobs, user=flask_login.current_user)


@app.route('/debug_log')
@flask_login.login_required
def debug_log():
    logs = job_db.all_log()
    logs = reversed(logs)
    return render_template('util/debug_logs.html', logs=logs, user=flask_login.current_user)


@app.route('/queue')
@flask_login.login_required
def queue():
    queue = redis_queue.get_queue()
    return render_template('util/queue.html', queue=queue, user=flask_login.current_user)


@app.route('/job_info')
@flask_login.login_required
def job_info():
    job_id = int(request.args.get('job_id'))
    job = job_db.get_job(job_id)
    logs = job_db.job_log(job_id)
    print(logs)
    return render_template('util/job_info.html', job=job, logs=logs, user=flask_login.current_user)


@app.route("/job_dashboard")
@flask_login.login_required
def job_dashboard():
    jobs = []
    for group in flask_login.current_user.groups:
        jobs.extend(job_db.get_job_by_group(group))
    jobs = sorted(jobs, key=lambda j: j.timestamp, reverse=True)
    return render_template('util/jobs.html', jobs=jobs, user=flask_login.current_user)


@app.route("/groups", methods=["GET", "POST"])
@flask_login.login_required
def groups():
    print(flask_login.current_user)
    if request.method == "GET":
        public = user_db.get_all_groups_by_mode(1)
        private = user_db.get_all_groups_by_mode(2)
        return render_template('util/groups.html', your_groups=flask_login.current_user.groups, public=public,
                               private=private, user=flask_login.current_user)
    group_mode = int(request.form.get('group_mode'))
    group_name = request.form.get('group_name')
    if group_name:
        log(f"create group: {group_name} mode: {group_mode}")
        user_db.add_group(group_name, group_mode, flask_login.current_user)
    return redirect("/groups")


@app.route("/group_info", methods=["GET", "POST"])
@flask_login.login_required
def group_info():
    if request.method == "GET":
        group_name = request.args.get('group_name')
        group = user_db.get_group(group_name)
        users = [u.name for u in user_db.get_users_group(group_name)]
        if not group:
            return redirect("/home")
        if flask_login.current_user.name not in users and group.mode == 3:
            return redirect("/home")
        return render_template('util/group_info.html', group=group, users=users, user=flask_login.current_user)

    group_name = request.args.get('group_name')
    group = user_db.get_group(group_name)
    if group.mode == 1:
        user = user_db.get_user(flask_login.current_user.name)
    else:
        username = request.form.get('username')
        user = user_db.get_user(username)
        if not user or group_name in user.groups:
            return redirect(f"/group_info?group_name={group_name}")
    user.groups.append(group_name)
    user_db.update_user(user)
    log(f"add user:{user.name} to group:{group_name}")
    return redirect(f"/group_info?group_name={group_name}")


@app.route("/log_info")
@flask_login.login_required
def log_info():
    log_id = int(request.args.get('log_id'))
    log = job_db.get_log(log_id)
    return render_template('util/log_info.html', log=log, user=flask_login.current_user)


@app.route("/healthy")
def healthy():
    return {'msg': "im good"}


@app.route('/login', methods=['GET', 'POST'])
def login():
    if flask_login.current_user.is_authenticated:
        return redirect(url_for('home'))

    if request.method == "GET":
        return render_template('util/login.html', text='')

    username = request.form.get('username')

    if not user_db.user_exist(username):
        return render_template('util/login.html', text='Wrong Username or Password')

    user = user_db.get_user(username)
    if not hashlib.sha224(request.form.get('pass').encode('utf-8')).hexdigest() == user.password:
        return render_template('util/login.html', text='Wrong Username or Password')

    flask_user = User()
    flask_user.__dict__.update(user.__dict__)
    flask_login.login_user(flask_user)
    log("login")
    return redirect(url_for('home'))


@app.route('/logout')
@flask_login.login_required
def logout():
    log("logout")
    flask_login.logout_user()
    return redirect(url_for('login'))



@app.route('/scraper', methods=['GET', 'POST'])
@flask_login.login_required
def start():
    if request.method == "GET":
        module_name = request.args.get("module")
        return render_template('scraper_generator.html', data=job_db.get_module(module_name).config,
                               user=flask_login.current_user)

    item = {}

    module_name = request.args.get('module')
    module = job_db.get_module(module_name).config
    group = request.form.get('group')

    prio = False if request.form.get('prio') != "1" else True
    schedule = 0 if request.form.get('prio') != "3" else datetime.datetime.strptime(request.form.get('date'),
                                                                                    '%Y-%m-%d').timestamp()
    recurrent = int(request.form.get('recurrent')) if request.form.get('recurrent') else 0
    data = {}
    for r in module.get('requirements'):
        value = request.form.get(r.get('name'))
        if not value:
            data[r.get('name')] = False
            continue
        if r.get('split'):
            new_value = []
            for v in value.split("\r\n"):
                if not v:
                    continue

                if r.get('type') == "int":
                    v = int(v)
                elif r.get('type') == "float":
                    v = float(v)
                new_value.append(v)
        else:
            if r.get('type') == "int":
                new_value = int(value)
            elif r.get('type') == "float":
                new_value = float(value)
            else:
                new_value = True
        data[r.get('name')] = new_value
    log(f"starting {module_name}: {data}")

    job_id = job_db.add_job(flask_login.current_user.name, module_name, group, data, prio=prio, recurrent=recurrent,
                            target=str(data.get(module.get('target'))))

    item['job_id'] = job_id
    item['module_name'] = module_name
    item['data'] = data
    item['recurrent'] = recurrent
    item['prio'] = prio
    item['schedule'] = schedule
    redis_queue.queue(item)
    threading.Thread(target=ping_job_queue).start()

    return redirect("home")


def ping_job_queue():
    try:
        requests.get(f'http://{os.environ["job_manager_host"]}:5001/wake_queue')
    except requests.exceptions.ConnectionError:
        traceback.print_exc()


#
@app.route("/download_job")
@check_user
def download_job():
    job_id = int(request.args.get('id'))
    job = job_db.get_job(job_id)
    log(f"download result from group:{job.group} job:{job_id}")

    if not job.group in flask_login.current_user.groups:
        log(f"ALERT user not in correct group: {','.join(flask_login.current_user.groups)}")
        return redirect("/home")

    out_path = os.path.join("temp", str(job_id))

    if not os.path.isfile(out_path + ".zip"):
        result_path = os.path.join("result_share", f"{job_id}")
        shutil.make_archive(out_path, 'zip', result_path)

    return send_from_directory('temp', f"{job_id}.zip", as_attachment=True, download_name=f"export.zip")


if __name__ == "__main__":
    # redis_queue.delete_queue()
    #  running the app at port 5000
    app.run(host='0.0.0.0', port=5000)  # , ssl_context=('server.crt', 'server.key'))  # , ssl_context='adhoc')
